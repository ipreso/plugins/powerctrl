<?
// Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
//
// This file is part of iPreso.
//
// iPreso is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation,
// either version 3 of the License, or (at your option) any
// later version.
//
// iPreso is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License
// for more details.
//
// You should have received a copy of the GNU General
// Public License along with iPreso. If not, see
// <https://www.gnu.org/licenses/>.
//

class Actions_Plugin_PowerctrlTable extends Zend_Db_Table
{
    protected $_name = 'Actions_powerctrl';

    public function setScheduledWakeup ($hash, $schedule, $wakeup)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['schedule']   = $schedule;
                $row ['wakeup']     = new Zend_Db_Expr("FROM_UNIXTIME($wakeup)");
                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    'schedule'  => $schedule,
                    'wakeup'    => new Zend_Db_Expr("FROM_UNIXTIME($wakeup)")));

        return ($hashConfirm);
    }

    public function wakeup ($hash, $date = "")
    {
        if (!strlen ($date))
            $date = "0000-00-00 00:00:00";

        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                if (strcmp ($date, "NULL") == 0)
                  $row ['wakeup']   = NULL;
                else
                  $row ['wakeup']   = new Zend_Db_Expr("FROM_UNIXTIME($date)");

                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    'wakeup'    => $date));

        return ($hashConfirm);
    }

    public function addBox ($hash)
    {
        $rowset = $this->find ($hash);
        if ($rowset && $rowset->current ())
            return ($hash);
        
        $hashConfirm = $this->insert (array ('hash' => $hash));
        return ($hashConfirm);
    }

    public function reboot ($hash, $date = "")
    {
        if (!strlen ($date))
            $date = "0000-00-00 00:00:00";
        
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                if (strcmp ($date, "NULL") == 0)
                  $row ['reboot']   = NULL;
                else
                  $row ['reboot']   = $date;

                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    'reboot'    => $date));

        return ($hashConfirm);
    }

    public function halt ($hash, $date = "")
    {
        if (!strlen ($date))
            $date = "0000-00-00 00:00:00";
        
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                if (strcmp ($date, "NULL") == 0)
                  $row ['halt']     = NULL;
                else
                  $row ['halt']     = $date;

                $row ['modified']   = 1;
                $row->save ();
                return ($hash);
            }
        }
        
        $hashConfirm = $this->insert (
            array ( 'hash'      => $hash,
                    'halt'      => $date));

        return ($hashConfirm);
    }

    public function deleteHash ($hash)
    {
        $where = $this->getAdapter()->quoteInto('hash = ?', $hash);
        $this->delete ($where);
    }

    public function getHash ($hash)
    {
        $select = $this->select ()
                        ->from (array ('t' => $this->_name),
                                array ('schedule',
                                        'unix' => new Zend_Db_Expr("UNIX_TIMESTAMP(wakeup)"),
                                        'reboot',
                                        'halt',
                                        'modified'))
                        ->where ('hash = ?', $hash);
        $row = $this->fetchRow ($select);
        if (!$row)
            return (false);

        return (array ( 'wakeup'    => $row ['unix'],
                        'schedule'  => $row ['schedule'],
                        'reboot'    => $row ['reboot'],
                        'halt'      => $row ['halt'],
                        'modified'  => $row ['modified']));
    }

    public function setRead ($hash)
    {
        $rowset = $this->find ($hash);
        if ($rowset)
        {
            $row = $rowset->current ();
            if ($row)
            {
                $row ['modified'] = 0;
                $row->save ();
            }
        }
        
        return (true);
    }

}

