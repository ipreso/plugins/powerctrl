CREATE TABLE IF NOT EXISTS Actions_powerctrl (
    `hash` varchar(100) NOT NULL,
    `schedule` varchar(100) DEFAULT NULL,
    `wakeup` datetime DEFAULT NULL,
    `halt` datetime DEFAULT NULL,
    `reboot` datetime DEFAULT NULL,
    `modified` int(2) DEFAULT 1,
    PRIMARY KEY  (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP PROCEDURE IF EXISTS modify_table;

DELIMITER |

CREATE PROCEDURE `modify_table` () DETERMINISTIC
BEGIN

    IF NOT EXISTS
    (
        SELECT 1
        FROM Information_schema.columns
        WHERE table_name = 'Actions_powerctrl'
            AND table_schema = 'iComposer'
            AND column_name = 'modified'
    ) THEN
        ALTER TABLE Actions_powerctrl ADD COLUMN modified int(2) DEFAULT 1;
    END IF;

    IF NOT EXISTS
    (
        SELECT 1
        FROM Information_schema.columns
        WHERE table_name = 'Actions_powerctrl'
            AND table_schema = 'iComposer'
            AND column_name = 'schedule'
    ) THEN
        ALTER TABLE Actions_powerctrl ADD COLUMN schedule varchar(100) DEFAULT NULL;
    END IF;

END|

DELIMITER ;


CALL modify_table ();

DROP PROCEDURE modify_table;

UPDATE Actions_powerctrl SET halt=NULL, reboot=NULL, modified=1;
