#!/bin/bash
# Copyright 2009-2021, Marc Simonetti <marc.simonetti@geekcorp.fr>
#
# This file is part of iPreso.
#
# iPreso is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public
# License as published by the Free Software Foundation,
# either version 3 of the License, or (at your option) any
# later version.
#
# iPreso is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General
# Public License along with iPreso. If not, see
# <https:#www.gnu.org/licenses/>.
#
# ====

SYNCHRO="/usr/bin/iSynchro"
SCHEDULE="/usr/bin/iSchedule"
AT="/usr/bin/at"
AT_QUEUE_HALT="i"
AT_QUEUE_REBOOT="j"

INTERFACE=$( grep license_net /etc/ipreso/iplayer.conf | \
                cut -d'=' -f2 | \
                awk '{print $1}' )


###############################################################################
# Log everything as player does
function iInfo
{
    logger -p local4.info -t Powerctrl -- "$@"
}
function iError
{
    logger -p local4.err -t Powerctrl -- "$@"
}

function usage
{
    echo "Usage:"
    echo "$0 <cmd>"
    echo ""
    echo "With <cmd> in:"
    echo "  -a                      Apply last configuration"
    echo "  -c                      Clean the power schedule"
    echo "  -h                      Display this usage screen"
    echo "  -r now                  Reboot the player"
    echo "  -s <soft|hard>          Shutdown the player"
    echo "  -u                      Update the next wake-up according to iSchedule"
    echo "  -w <timestamp>          Wake-up the player"
    echo "  -w no                   Disable wake-up"
}

function getMyFullPath
{
    LSOF=$(lsof -p $$ | grep -E "/"$(basename $0)"$")
    MY_PATH=$(echo $LSOF | sed -r s/'^([^\/]+)\/'/'\/'/1 2>/dev/null)
    echo $MY_PATH
}

function getMyConfFile
{
    DIR=$( dirname $(getMyFullPath) )
    echo "$DIR/powerctrl.conf"
}

function parseConfiguration
{
    if [ ! -f "${CONFFILE}" ] ; then
        CONF_WAKEUP=""
        return
    fi

    CONF_WAKEUP=$( grep -E '^WAKEUP' ${CONFFILE} | sed -r 's/^WAKEUP=//g' )
}

function saveConfiguration
{
    CONFIGURATION="WAKEUP=${CONF_WAKEUP}"

    # Overwriting configuration file with current variables
    echo "${CONFIGURATION}" | sudo /usr/bin/tee "${CONFFILE}" &>/dev/null
    if [ "$?" != "0" ] ; then
        echo "Unable to write configuration in ${CONFFILE}."
        return 1
    fi
    return 0
}

function failure
{
    iError "$1"
    exit 1
}

function checkTimestamp
{
    if [ -z "$1" ] ; then
        echo "Timestamp is empty"
        return 1
    fi

    NOW=$( date +%s )
    if [ "$NOW" -gt "$1" ] ; then
        echo "$1 is in the past (now: $NOW)"
        return 1
    fi

    return 0
}

function synchroWakeup
{
    NEXT_WAKEUP_TIMESTAMP=$( cat /sys/class/rtc/rtc0/wakealarm )
    if [ -z "${NEXT_WAKEUP_TIMESTAMP}" ] ; then
        sendToComposer "Configured Wake-Up: NONE"
    else
        NEXT_WAKEUP_HUMAN=$( date -d @${NEXT_WAKEUP_TIMESTAMP} +%F" "%T )
        sendToComposer "Configured Wake-Up: ${NEXT_WAKEUP_HUMAN}"
    fi
}

function sendToComposer
{
    # Check that we have an IP for the connectivity
    IP_ADDR=$( ip a list dev ${INTERFACE} | grep inet )
    if [ -z "${IP_ADDR}" ] ; then
        iError "Cannot send Powerctrl/$1 to Composer: no connectivity."
    else
        iInfo "Sending to Composer Powerctrl/$1"
        ${SYNCHRO} --send "Powerctrl/$1"
    fi
}

###############################################################################
function haltNow
{
    # Warn Composer we are shutting down
    sendToComposer "halt_ok"

    # Send the next Wakup
    synchroWakeup

    # Log appropriate message (triggering or already triggered)
    if [ "$1" = "soft" ] ; then
        # System is halting (systemd is stopping services)
        # - Warn the Composer
        iInfo "Player is shutting down, now."
        sendToComposer "Player is shutting down, now."
    else
        # Ask the system to halt now !
        # - First, log and warn the Composer
        iInfo "Shut-Off asked by Composer or Schedule."
        sendToComposer "Triggering Shut-Off."
        # - Then, effective shutdown command
        sudo /sbin/shutdown -h now
    fi

    return 0
}

function rebootNow
{
    # Warn Composer we are rebooting
    sendToComposer "reboot_ok"

    # Ask the system to reboot now !
    # - First, log and warn the Composer
    iInfo "Reboot asked by Composer or Schedule."
    sendToComposer "Triggering Reboot."
    # - Then, effective shutdown command
    sudo /sbin/shutdown -r now

    return 0
}

function setWakeupFromSchedule
{
    # Update the Wakealarm according to the current schedule

    # This method is allowing boxes to boot everyday without
    # having to query the Composer

    # Get the next timestamp according to iSchedule
    NEXT_WAKEUP=$( ${SCHEDULE} --show | 
                    grep "powerctrl.cmd -w now" | 
                    head -n1 | cut -d' ' -f1,2 | 
                    sed -r 's/\./-/g' | 
                    awk '{print "date -d \""$0"\" +%s" }' | sh )

    # If something is wrong for the date format (empty string)
    # the returned timestamp is "today 00:00:00"

    iInfo "Next iSchedule Wakeup: ${NEXT_WAKEUP}"
    setWakeup ${NEXT_WAKEUP}
}

function setWakeup
{
    # Write permission on this file (for user player) is set with 
    # iLaunch-pre service unit
    echo "0" > /sys/class/rtc/rtc0/wakealarm

    if [ "$1" == "no" ];
    then
        iInfo "Disabling Wakeup RTC alarm."
        CONF_WAKEUP=""
    else
        RES=$( checkTimestamp $1 ) || failure "${RES}"

        iInfo "Setting Wakeup RTC alarm at: $1"
        CONF_WAKEUP=$1

        echo "$1" > /sys/class/rtc/rtc0/wakealarm

        iInfo "Forcing clock hardware synchronization after alarm setting"
        /usr/bin/synchronizeTime_hw_only

        NEXT_WAKEUP_TIMESTAMP=$( cat /sys/class/rtc/rtc0/wakealarm )
        NEXT_WAKEUP_HUMAN=$( date -d @${NEXT_WAKEUP_TIMESTAMP} +%F" "%T )
        iInfo "RTC Alarm configured at: ${NEXT_WAKEUP_HUMAN}"
    fi

    # Saving this new Wakeup configuration
    RES=$( saveConfiguration ) || failure "${RES}"
    synchroWakeup

    return 0
}

###############################################################################

# This script use player permissions, even if launched as root.
if [ "${EUID}" = "0" ];
then
    iInfo "Running $( basename $0 ) as user 'player'..."
    if [[ x = x$@ ]] ; then
        sudo -u player $0
    else
        sudo -u player $0 "$@"
    fi
    exit $?
fi

CONFFILE="$( getMyConfFile )"
SCRIPTFILE="$( getMyFullPath )"

# Parse and load current configuration
parseConfiguration

# What should we do ?
flag=
bflag=
while getopts 'achs:r:uw:' OPTION ; do
    case ${OPTION} in
        a)
            iInfo "Applying last known configuration..."
            #if [ -n "${CONF_HALT}" ] ; then
            #    setHalt "${CONF_HALT}"
            #else
            #    setHalt "no"
            #fi
            #if [ -n "${CONF_REBOOT}" ] ; then
            #    setReboot "${CONF_REBOOT}"
            #else
            #    setReboot "no"
            #fi
            if [ -n "${CONF_WAKEUP}" ] ; then
                setWakeup "${CONF_WAKEUP}"
            else
                setWakeup "no"
            fi
            ;;
        c)
            iInfo "Cleaning the Powerctrl schedule..."
            #CONF_HALT=""
            #setHalt "no"
            #CONF_REBOOT=""
            #setReboot "no"
            CONF_WAKEUP=""
            setWakeup "no"
            saveConfiguration
            ;;
        h)
            usage $( basename $0 )
            exit 0
            ;;
        s)
            TIME=${OPTARG}
            if [ "${TIME}" = "now" -o "${TIME}" = "hard" ] ; then
                # Shutdown now ! (planned or asked by Composer)
                haltNow "hard"
            elif [ "${TIME}" = "soft" ] ; then
                # Shutdown in progress (called by systemd)
                haltNow "soft"
            #else
            #    # Planning a new shut-off time
            #    setHalt "${TIME}"
            fi
            ;;
        r)
            TIME=${OPTARG}
            if [ "${TIME}" = "now" ] ; then
                # Shutdown now ! (planned or asked by Composer)
                rebootNow
            #else
            #    # Planning a new shut-off time
            #    setReboot "${TIME}"
            fi
            ;;
        u)
            setWakeupFromSchedule
            ;;
        w)
            setWakeup ${OPTARG}
            ;;
        *)
            usage $( basename $0 )
            exit 2
            ;;
    esac
done
